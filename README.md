GenMan is a genetics simulator for a text adventure game I'm building.
It will have support for trait crossing, random mutations, organism handling, etc.
It should be compatible with all compilers.

Though I doubt anyone would want to use this hunk o' junk, it is licensed under the BSD 3 Revised license.