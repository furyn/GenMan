#include <iostream>
#include <string>
#include <map>

#include "organism.h"

using namespace std;

void NewOrganism(map<string, Organism> &MMap, string oName, string oSpecies, int oSex) {
    Organism tmp;
    tmp.Init(oName, oSpecies);
    tmp.sex = oSex;
    MMap[oName] = tmp;
}

int main() {
    map<string, Organism> OMap;
    NewOrganism(OMap, "Watusi", "Retriever", 1);
    OMap["Watusi"].AddTrait("Black Fur", false, 0, 0);

    NewOrganism(OMap, "Dog2", "Retriever", 1);
    OMap["Dog2"].AddTrait("White Fur", false, 0, 0);

    OMap["Dog2"].Breed(OMap["Watusi"], "Off");

    return 0;
}
