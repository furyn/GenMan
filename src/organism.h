#pragma once
#include <map>

#include "trait.h"

#ifndef ORGANISM_H
#define ORGANISM_H
using namespace std;
class Organism
{
    public:
        Organism();
        ~Organism();

        Trait GetTrait(string key);
        void AddTrait(string key, bool pDom, int a, int b);
        void Init(string pName, string pSpecies);
        Organism Breed(Organism so, string pName);

        map<string, Trait> tMap;

        int sex;
        string name;
        string species;


    private:
};

#endif // ORGANISM_H
