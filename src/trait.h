#pragma once
#ifndef TRAIT_H
#define TRAIT_H
#include <string>

using std::string;

class Trait {
public:
    Trait();
    ~Trait();


    string name; // Name of the trait.
    string desc; // Description of the trait itself.
    string flavText; // Text for use in a text game.


    // These 2 are used to determine genotype. Set each to either 1 or 0
    // for dominant or recessive
    int A; // Allele 1
    int B; // Allele 2

    // Tells whether the appearance of the trait is dominant
    bool isDom;
};

#endif // TRAIT_H
